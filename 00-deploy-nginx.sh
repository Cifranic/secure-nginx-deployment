##SOurces
##https://geekflare.com/install-modsecurity-on-nginx/
## Tested on AWS AMI amazon-linux (centos variant) 


#### INSTALL mod securiy, git

yum -y install epel-release 

# grab wget, git and openssl 
yum -y install wget git openssl-devel

# dependencies for making binaries from scratch 
yum -y install gcc make automake autoconf libtool pcre pcre-devel libxml2 libxml2-devel curl curl-devel httpd-devel

# move to home 
cd ~

# pull my repo 
git clone https://gitlab.com/Cifranic/secure-nginx-deployment.git

# grab nginx source code (version 1.16.1 is most up to date)
wget https://nginx.org/download/nginx-1.16.1.tar.gz

# unzip
gunzip -c nginx-1.16.1.tar.gz| tar xvf -

# get most up to date version of modsecuity (2.9.3)
wget https://www.modsecurity.org/tarball/2.9.3/modsecurity-2.9.3.tar.gz

# unzip it
gunzip -c modsecurity-2.9.3.tar.gz | tar xvf -

# go into modsec dir and build binary
cd ~/modsecurity-2.9.3
./configure --enable-standalone-module
make

# go into nginx dir and build binary 
cd ~/nginx-1.16.1/
./configure --add-module=../modsecurity-2.9.3/nginx/modsecurity --with-http_ssl_module
make
make install

# copy important files to nginx web-root
cp /root/modsecurity-2.9.3/unicode.mapping /usr/local/nginx/conf/
cp /root/modsecurity-2.9.3/modsecurity.conf-recommended /usr/local/nginx/conf/

# rename file to modsecurity.conf
mv /usr/local/nginx/conf/modsecurity.conf-recommended /usr/local/nginx/conf/modsecurity.conf

# copy service file for systemd 
cp ~/secure-nginx-deployment/system-files/nginx.service /usr/lib/systemd/system/

# reload systemd 
systemctl daemon-reload 

# start nginx
systemctl start nginx 

# ensure that modsecurity plugin for nginx works:  "modsecurity successful"
tail /usr/local/nginx/logs/error.log 

### CONFIGURE Mod-security and add OWASP rules 
cd ~
git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git

# create one BIG rules file in nginx/conf directory
cat ~/owasp-modsecurity-crs/crs-setup.conf.example ~/owasp-modsecurity-crs/rules/*.conf >> /usr/local/nginx/conf/modsecurity.conf

# copy over rule to nginx/conf directory
cp ~/owasp-modsecurity-crs/rules/*.data /usr/local/nginx/conf/

# copy proper nginx.conf file 
cp ~/secure-nginx-deployment/system-files/nginx.conf /usr/local/nginx/conf/nginx.conf


# What the next command does: 
    # enables Intrusion Prevention
    # Changes file: /usr/local/nginx/conf/modsecurity.conf
       #FROM: "SecRuleEngine Detect" 
       #TO:   "SecRuleEngine On"

       #FROM: SecDefaultAction "phase:1,log,auditlog,pass" TO 
       #TO:   SecDefaultAction "phase:1,deny,log"

       #NOTE:  Changing SecDefaultAction to deny, and log is where modsec is instructed to prevent intrusions
cp /root/secure-nginx-deployment/system-files/modsecurity.conf /usr/local/nginx/conf/modsecurity.conf

# restart nginx after modsecurity conf file is updated 
systemctl restart nginx

# MANUAL TESTING SECTION 

# verify nginx installed correctly, and using modSec plugin:
/usr/local/nginx/sbin/nginx -V 

#SHOULD SEE THIS OUTPUT:
  #nginx version: nginx/1.16.1
  #built by gcc 7.3.1 20180712 (Red Hat 7.3.1-6) (GCC) 
  #built with OpenSSL 1.0.2k-fips  26 Jan 2017
  #TLS SNI support enabled
  #configure arguments: --add-module=../modsecurity-2.9.3/nginx/modsecurity --with-http_ssl_module


# In another terminal window, Watch the modsec audit file fill up, detect and prevent actions
tail -f /var/log/modsec_audit.log

# Example Action 1, prevent direct connections to IP address; block unless domain name is queried:
curl <nginx_SERVER_IP>

# Action 1 curl output: 
#<html>
#<head><title>403 Forbidden</title></head>
#<body>
#<center><h1>403 Forbidden</h1></center>
#<hr><center>nginx/1.16.1</center>
#</body>
#</html>

# Action 1 modsec audit log output:
#GET / HTTP/1.1
#Host: 34.220.144.30
#User-Agent: curl/7.61.1
#Accept: */*

#--c78f484c-F--
#HTTP/1.1 403
#Content-Type: text/html
#Content-Length: 153
#Connection: keep-alive

#--c78f484c-H--
#Message: Access denied with code 403 (phase 2). Pattern match "^[\\d.:]+$" at REQUEST_HEADERS:Host. [file "/usr/local/nginx/conf/modsecurity.conf"] [line "5432"] [id "920350"] [msg "Host header is a numeric IP address"] [data "34.220.144.30"] [severity "WARNING"] [ver "OWASP_CRS/3.2.0"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-protocol"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "OWASP_CRS/PROTOCOL_VIOLATION/IP_HOST"] [tag "WASCTC/WASC-21"] [tag "OWASP_TOP_10/A7"] [tag "PCI/6.5.10"]
#Message: Audit log: Failed to lock global mutex: Permission denied
#Action: Intercepted (phase 2)
#Apache-Handler: IIS
#Stopwatch: 1586536116000158 159054 (- - -)
#Stopwatch2: 1586536116000158 159054; combined=625, p1=381, p2=163, p3=0, p4=0, p5=81, sr=76, sw=0, l=0, gc=0
#Producer: ModSecurity for nginx (STABLE)/2.9.3 (http://www.modsecurity.org/); OWASP_CRS/3.2.0.
#Server: ModSecurity Standalone
#Engine-Mode: "ENABLED"

#--c78f484c-Z--


# Example Action 2, prevent web requests trying to inject shell to HTTP post (insert proper domain name):
curl https://nginx.popcorn.blue/index.html?exec=/bin/bash

# OUTPUT of ModSec audit log: 
#[09/Apr/2020:19:07:47 +0000] AcAcicwcAPWcAcA9tcAcAcpR 76.172.91.231 53443 127.0.0.1 80
#--cf1e9827-B--
#GET /index.html?exec=/bin/bash HTTP/1.1
#Host: 34.212.70.58
#User-Agent: curl/7.64.1
#Accept: */*

#--cf1e9827-F--
#HTTP/1.1 403
#Content-Type: text/html
#Content-Length: 153
#Connection: keep-alive

#--cf1e9827-H--
#Message: Access denied with code 403 (phase 2). Matched phrase "bin/bash" at ARGS:exec. [file "/usr/local/nginx/conf/modsecurity.conf"] [line "7421"] [id "932160"] [msg "Remote Command Execution: Unix Shell Code Found"] [data "Matched Data: bin/bash foun
#d within ARGS:exec: /bin/bash"] [severity "CRITICAL"] [ver "OWASP_CRS/3.2.0"] [tag "application-multi"] [tag "language-shell"] [tag "platform-unix"] [tag "attack-rce"] [tag "paranoia-level/1"] [tag "OWASP_CRS"] [tag "OWASP_CRS/WEB_ATTACK/COMMAND_INJECTIO
#N"] [tag "WASCTC/WASC-31"] [tag "OWASP_TOP_10/A1"] [tag "PCI/6.5.2"]
#Message: Audit log: Failed to lock global mutex: Permission denied
#Action: Intercepted (phase 2)
#Apache-Handler: IIS
#Stopwatch: 1586459267000384 385701 (- - -)
#Stopwatch2: 1586459267000384 385701; combined=856, p1=385, p2=391, p3=0, p4=0, p5=79, sr=91, sw=1, l=0, gc=0
#Producer: ModSecurity for nginx (STABLE)/2.9.3 (http://www.modsecurity.org/); OWASP_CRS/3.2.0.
#Server: ModSecurity Standalone
#Engine-Mode: "ENABLED"


### Troubleshooting:

# Logs
#  ModSec Audit log (block log): /var/log/modsec_audit.log
#  Nginx Logs:  /usr/local/nginx/logs/
