# this script installs the certs 
# source: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-centos-7

# get dependencies:
yum -y install epel-release
yum -y install python2-certbot-nginx

# change "server_name" variable to your domain name
vi /usr/local/nginx/conf/nginx.conf

  #server_name _;
  #server_name nginx.popcorn.blue www.nginx.popcorn.blue;

# allow certbox to update nginx.conf
certbot --nginx --nginx-server-root /usr/local/nginx/conf --nginx-ctl /usr/local/nginx/sbin/nginx -d nginx.popcorn.blue -d www.nginx.popcorn.blue